##############################################################################
############ Startup File for DTL Cavities Master1
############
############ Authors: Maurizio Montis || INFN-LNL
############          Alfio Rizzo || ESS
############ Mail:   maurizio.montis@lnl.infn.it
############         alfio.rizzo@ess..eu
##############################################################################

require dtlcavm1
require essioc
require ecmccfg, 7.0.1
require mcoreutils


mcoreThreadRuleAdd ecmc * * 1 ecmc_rt


# Register our local db directory
epicsEnvSet(DB_DIR, "$(E3_CMD_TOP)/db/")

# Register our local iocsh directory
epicsEnvSet(IOCSH_DIR, "$(E3_CMD_TOP)/iocsh/")

# Ethercat Master Name and Index
epicsEnvSet("MASTERNAME", "DTL:Ctrl-ECAT-001")
epicsEnvSet("MASTERID","2")

# Load Common Modules
iocshLoad("$(essioc_DIR)common_config.iocsh")

# Configure Master1 Slaves
iocshLoad("$(IOCSH_DIR)dtl-ethercat-m1-cfg.iocsh")

# Map ecmccfg Analog (RTD) PVs

iocshLoad("$(IOCSH_DIR)dtl-thermosensors-pv-map.iocsh")

# Map ecmccfg Digital IO PVs
iocshLoad("$(IOCSH_DIR)dtl-digitalIO-pv-map.iocsh")

# Temperature System - Steerer System - Maintenance Reset
dbLoadRecords("$(DB_DIR)dtl-reset-maintenance.db")

# Steerer System - Total Ok Status
dbLoadRecords("$(DB_DIR)dtl-steerer-totalok-stat.db")

###############################################################################
############## Post Configuration - Record Fields Config
# Thermosensor Alarm Bypass

iocshLoad("$(IOCSH_DIR)dtl-thermosensor-bypass-postcfg.iocsh")


###############################################################################
############## Post Configuration - Load State Machines (Temperature/Steerer)

iocshLoad("$(dtlcavm1_DIR)dtl-statemachine-code-pv.iocsh")
iocshLoad("$(dtlcavm1_DIR)postInitStateMachine-cfg.iocsh") 

