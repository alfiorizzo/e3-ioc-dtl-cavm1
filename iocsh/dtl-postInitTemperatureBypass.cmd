#==============================================================================
# postInitTemperatureAlarm.cmd
#- Arguments: P, IDX, LOLO, LLSV, LOW, LSV, HIGH, HSV, HIHI, HHSV, HYST, HOPR, LOPR

#-d /**
#-d   \brief Configure Record fields for DTL temperature sensor
#-d   \details Set Alarm values and severity according to the thermosensor system design.
#-d   \author Maurizio Montis (INFN-LNL)
#-d   \file
#-d   \param P System name, i.e. DTL-010
#-d   \param IDX Device position (accordig to ESS Naming Convetion indexing)
#-d   \param LOLO Lowest alarm value
#-d   \param LLSV Lowest alarm severity, i.e. MAJOR
#-d   \param LOW Low alarm value
#-d   \param LSV Low alarm severity, i.e. MINOR
#-d   \param HIGH High alarm value
#-d   \param HSV HIgh alarm severity, i.e. MAJOR
#-d   \param HIHI Highest alarm value
#-d   \param HHSV Highest alarm severity, i.e. MAJOR
#-d   \param HYST Alarm hysteresis value
#-d   \param HOPR High operator range
#-d   \param LOPR Low operator range 
#-d   \note Example call:
#-d   \code
#-d    ${SCRIPTEXEC} "$(dtlThermo_DIR)postInitTemperatureAlarm.cmd", "P=DTL-010, IDX=001, LOLO=0, LLSV=NO_ALARM, LOW=0, LSV=NO_ALARM, HIGH=30, HSV=MINOR, HIHI=35, HHSV=MAJOR, HYST=5, HOPR=150, LOPR=10"
#-d   \endcode
#-d */



# bypass
afterInit(dbpf "$(P):EMR-TT-$(IDX):Temp.SDIS" "$(P):EMR-TT-$(IDX):TempDis")
afterInit(dbpf "$(P):EMR-TT-$(IDX):Temp.DISV" "1")
